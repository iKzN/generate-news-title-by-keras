FROM python:3.8-slim

COPY . /root

WORKDIR /root

RUN pip install flask gunicorn numpy sklearn scipy pandas flask_wtf python-dotenv xlrd openpyxl tensorflow==2.3.0 matplotlib