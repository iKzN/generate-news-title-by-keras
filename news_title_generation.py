import os
import joblib
import numpy as np
import pandas as pd
import json
import re

from flask import Flask, request, flash, jsonify, abort, redirect, url_for, render_template, send_file, after_this_request
from flask_wtf import FlaskForm
from markupsafe import escape
from wtforms import StringField, FileField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename

import tensorflow as tf
from tensorflow import keras

app = Flask(__name__)

# выбор настроек среды(в данный момент почти пустой конфиг)
app.config.from_object('config.DevConfig')

# массив идентификаторов символов(из обучения чтобы правильно декодировать и кодировать)
json_char = '{" ": 0, ",": 1, "-": 2, ".": 3, "b": 4, "c": 5, "d": 6, "e": 7, "f": 8, "g": 9, "h": 10, "i": 11, "j": 12, "k": 13, "l": 14, "m": 15, "o": 16, "p": 17, "q": 18, "r": 19, "s": 20, "t": 21, "u": 22, "v": 23, "w": 24, "x": 25, "y": 26, "z": 27, "а": 28, "б": 29, "в": 30, "г": 31, "д": 32, "е": 33, "ж": 34, "з": 35, "и": 36, "й": 37, "к": 38, "л": 39, "м": 40, "н": 41, "о": 42, "п": 43, "р": 44, "с": 45, "т": 46, "у": 47, "ф": 48, "х": 49, "ц": 50, "ч": 51, "ш": 52, "щ": 53, "ъ": 54, "ы": 55, "ь": 56, "э": 57, "ю": 58, "я": 59}'

char2idx = json.loads(json_char)
idx2char = list(char2idx.keys())

# функция построения модели
def build_model(vocab_size, embedding_dim, rnn_units_first, rnn_units_second, batch_size):
    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(vocab_size, embedding_dim,
                batch_input_shape=[batch_size, None]),
        tf.keras.layers.LSTM(rnn_units_first,
                return_sequences=True,
                stateful=True,
                recurrent_initializer='glorot_uniform'),
        tf.keras.layers.LSTM(rnn_units_second,
                return_sequences=True,
                stateful=True,
                recurrent_initializer='glorot_uniform'),
        tf.keras.layers.Dense(embedding_dim),
        tf.keras.layers.Dense(vocab_size)
    ])
    return model

# создание и загрузка весов
generation_model = build_model(
    vocab_size = 60,
    embedding_dim=512,
    rnn_units_first=1024,
    rnn_units_second=1024,
    batch_size=1)

generation_model.load_weights("model/test_model_for_docker_weights_120")
generation_model.build(tf.TensorShape([1, None]))

# веб-интерфейс
@app.route('/')
def clear_format():
    return render_template('text_form.html')

# путь для запроса
@app.route('/get_text', methods = ['POST'])
def load_paint():
    """Return answer"""
    try:
        content = request.get_json()
        predict = generate_text(generation_model, start_string=text_clear(content['input_text']))
    except Exception as e:
        print('error')
        print(e)
        return redirect(url_for('bad_request'))

    return jsonify(predict)

# страница ошибки
@app.route('/badrequest')
def bad_request():
    return abort(400)

# вспомогательные функции

# очистка текста
def text_clear(text):
    result = text.lower()
    result = re.sub(r'[!?]', '.', result)
    result = re.sub(r'[^а-яa-z\.\-\,]', ' ', result)
    result = re.sub(r"[\|/]", '', result) 
    result = re.sub(r"[\n]", ' ', result) 
    result = re.sub(r"[\r]", ' ', result) 
    result = re.sub(r"[nan]", ' ', result) 
    result = re.sub(r'\s+', ' ', result)
    result = re.sub(r' \.', '.', result)
    result = re.sub(r'\. \.', '.', result)
    result = re.sub(r'\.\.', '.', result)
    return result

# генерация текста
def generate_text(model, start_string):
    # число сгенерированных символов
    num_generate = 100

    # преобразование введенной строки в текст
    input_eval = [char2idx[s] for s in start_string]
    input_eval = tf.expand_dims(input_eval, 0)

    # результат
    text_generated = []

    # параметр точности предсказания(добавляет некоторую случайность в ответ, чем выше значение тем больше случайность)
    temperature = 0.25

    model.reset_states()
    for i in range(num_generate):
        predictions = model(input_eval)
        # убираем размерность обучения по частям
        predictions = tf.squeeze(predictions, 0)

        # используем небольшую необпределенность для ответов, в случае необходимости постоянно одинакового и точного предсказания можно раскомментировать последнюю строчку
        predictions = predictions / temperature
        predicted_id = tf.random.categorical(predictions, num_samples=1)[-1,0].numpy()
        # predicted_id = np.argmax(predictions[0])

        # генерируем следующий входной набор данных
        input_eval = tf.expand_dims([predicted_id], 0)

        text_generated.append(idx2char[predicted_id])

    return (start_string + ''.join(text_generated))
